Required external deps
======================

bin/llbmc

bin/opt-2.6
bin/llvm-dis-2.9
bin/opt-2.6

bin/aigtoaig
bin/aigtocnf
bin/boolector
bin/synthebtor
bin/muser-2
bin/ufo

bin/z3
bin/z3.pyc
bin/z3_utils.pyc
bin/z3consts.pyc
bin/z3core.pyc
bin/z3num.pyc
bin/z3pdr.pyc
bin/z3poly.pyc
bin/z3printer.pyc
bin/z3rcf.pyc
bin/z3test.pyc
bin/z3types.pyc
bin/libz3.so

bin/llvm-gcc
bin/cilly.asm.exe

lib/libUfoFe.so

