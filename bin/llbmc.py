#!/usr/bin/env python

import sys
import os
import os.path
import atexit
import tempfile
import shutil
import subprocess as sub
import threading
import signal
import resource 
import time

root = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
verbose = True
running_process = None

os.environ['PATH'] = os.path.join (root, 'bin') + os.pathsep + os.environ['PATH']

def isexec (fpath):
    if fpath == None: return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK) 

def which(program):
    fpath, fname = os.path.split(program)
    if fpath:
        if isexec (program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if isexec (exe_file):
                return exe_file
    return None

def kill (proc):
    try:
        proc.terminate ()
        proc.kill ()
        proc.wait ()
        global running_process
        running_process = None
    except OSError:
        pass

def cat (in_file, out_file): out_file.write (in_file.read ())


def parseOpt (argv):
    from optparse import OptionParser

    parser = OptionParser ()
    parser.add_option ('--spec', help='Specification file', default=None)
    parser.add_option ('--cex', help='Destination for counterexample', default=None)
    parser.add_option ("--save-temps", dest="save_temps",
                       help="Do not delete temporary files",
                       action="store_true",
                       default=False)
    parser.add_option ("--temp-dir", dest="temp_dir",
                       help="Temporary directory",
                       default=None)
    parser.add_option ('--cpu', type='int', dest='cpu',
                       help='CPU time limit (seconds)', default=-1)
    parser.add_option ('--mem', type='int', dest='mem',
                       help='MEM limit (MB)', default=-1)
    parser.add_option ('--max-loop', dest='max_loop', type='int', default=-1,
                       help='Max loop unrolling bound')
    parser.add_option ('--delay', dest='delay', type=int, default=0,
                       help='Delay (in secs)'
                       )
    (options, args) = parser.parse_args (argv)

    if options.cex != None:
        if os.path.isfile (options.cex): os.remove (options.cex)
        
    return (options, args)
    
def createWorkDir (dname = None, save = False):    
    if dname == None:
        workdir = tempfile.mkdtemp (prefix='ufo-')
    else:
        workdir = dname

    if verbose:
        print "Working directory", workdir

    if not save:
        atexit.register (shutil.rmtree, path=workdir)
    return workdir

def getLlbmc ():
    llbmc = which ("llbmc")
    if llbmc is None:
        raise IOError ("Cannot find llbmc")
    return llbmc

def getOpt26 ():
    v = which ('opt-2.6')
    if v is None:
        raise IOError ('Cannot find opt-2.6')
    return v

def getLlvmDis29 ():
    v = which ('llvm-dis-2.9')
    if v is None: raise IOError ('Cannot find llvm-dis-2.9')
    return v

def getLlvmAs32 ():
    v = which ('llvm-as-3.2')
    if v is None: raise IOError ('Cannot find llvm-as-3.2')
    return v

def killall ():
    global running_process
    if running_process != None:
        running_process.terminate ()
        running_process.kill ()
        running_process.wait ()
        running_process = None

def sharedLib (base):
    ext = '.so'
    if sys.platform.startswith ('darwin'): ext = '.dylib'
    return base + ext

def toLlvm32 (oldbc, newbc):

    ufofeLib = sharedLib ('lib/libUfoFe')
    ufofeLib = os.path.join (root, ufofeLib)

    optArg = [getOpt26 (), '-load', ufofeLib, '-kill-nondet', '-raiseallocs', '-fixll']
    disArg = [getLlvmDis29 ()]
    asmArg = [getLlvmAs32 (), '-o', newbc]

    if verbose:
        print ' '.join (optArg), '<', oldbc, '|'
        print ' '.join (disArg), '|'
        print ' '.join (asmArg)

    opt = sub.Popen (optArg, stdin=open (oldbc), stdout=sub.PIPE)
    dis = sub.Popen (disArg, stdin=opt.stdout, stdout=sub.PIPE)
    asm = sub.Popen (asmArg, stdin=dis.stdout, stdout=sub.PIPE)
    dis.stdout.close ()
    output = asm.communicate () [0]

    if asm.returncode != 0:
        raise sub.CalledProcessError (asm.returncode, asmArg)

def is_llbmc_opt (x):
    llbmc_opt = ['max-builtins-iterations', \
                 'max-function-call-depth', \
                 'max-loop-iterations', \
                 'report', \
                 'result', \
                 'counterexample', \
                 'no-max-loop-iterations-checks', \
                 'ignore-undetermined-functions', \
                 'ignore-missing-function-bodies', \
                 'ignore-inline-assembly', 
                 'uninitialized-globals' ]
    if x.startswith ('-'):
        y = x.strip ('-')
        return y in llbmc_opt
    return False

def llbmc_result (f):
    if f is None: return None

    line = ""
    lines = open (f, 'rb')
    for l in lines : line = l
    if verbose: print 'LLBMC:', line
    if line.startswith ('Error detected.'): return 1
    #if line.startswith ('No error detected.') : return 0 
    return 2 # i.e. UNKNOWN
    
def llbmc (in_file, loop_bound=0, args = [], stdout_name=None, cpu = -1, mem = -1):
    def set_limits ():
        if mem > 0:
            mem_bytes = mem * 1024 * 1024
            resource.setrlimit (resource.RLIMIT_AS, [mem_bytes, mem_bytes])
    llbmc_args = [getLlbmc (),  '-only-custom-assertions', \
                  '-no-max-loop-iterations-checks', \
                  '-ignore-missing-function-bodies', \
                  '-enforce-max-loop-iterations', \
                  '-ignore-inline-assembly', \
                  '-ignore-undetermined-functions', \
                  '-counterexample', \
                  '-max-loop-iterations={}'.format(loop_bound), \
                  in_file]
    llbmc_args.extend (args)
    llbmc_args.append ('-result')
    if verbose: print ' '.join (llbmc_args)
    
    stdout=None
    if stdout_name <> None: stdout = open (stdout_name, 'wb')
    
    p = sub.Popen (llbmc_args, preexec_fn=set_limits, stdout=stdout)
    global running_process
    running_process = p

    timer = threading.Timer (cpu, kill, [p])
    if cpu > 0: timer.start ()
    
    try:
        (pid, returnvalue, ru_child) = os.wait4 (p.pid, 0)
        running_process = None
    except OSError: 
        pass     
    finally:
        ## kill the timer if the process has terminated already
        if timer.isAlive (): timer.cancel ()
    
    return llbmc_result (stdout_name)

def main (argv):
    llbmc_args = filter (is_llbmc_opt, argv[1:])
    argv = filter (lambda x : not is_llbmc_opt(x), argv [1:])
    (opt, args) = parseOpt (argv)

    if opt.delay > 0:
        if verbose: print 'Delaying execution for %d sec' % opt.delay
        time.sleep(opt.delay)
    if verbose: print 'Starting ,,,'
     
    workdir = createWorkDir (opt.temp_dir, opt.save_temps)

    assert len(args) == 1
    old_in_name = args [0]
    in_name = os.path.basename (args [0])
    in_name = os.path.splitext (in_name)[0]
    in_name = in_name + '.llvm32.bc'
    in_name = os.path.join (workdir, in_name)

    if verbose: print 'Recompiling to llvm 3.2...'
    toLlvm32 (old_in_name, in_name)
        
    if opt.max_loop < 0: opt.max_loop = 100000
    for i in range (0, opt.max_loop):
        print 'Running llbmc for bound', i, '...'
        stdout_name = os.path.join (workdir, 'llbmc.{}.stdout'.format (i))
        res = llbmc (in_name, i, llbmc_args, stdout_name)
        if res == 1: 
            print 'BRUNCH_STAT Result FALSE'
            if opt.cex is not None: cat (open(stdout_name, 'rb'), 
                                         open (opt.cex, 'wb'))
            return 0 # 0 means UNSAFE
    
    # if got here, the result is UNKNOWN
    return 2

def nothing (x, y):
    pass

if __name__ == '__main__':
    # unbuffered output
    sys.stdout = os.fdopen (sys.stdout.fileno (), 'w', 0)
    try:
        print 'BRUNCH_STAT Result UNKNOWN'
        signal.signal (signal.SIGTERM, lambda x, y: killall ())
        signal.signal (signal.SIGALRM, nothing)
        sys.exit (main (sys.argv))
    except KeyboardInterrupt: pass
    finally: killall ()


