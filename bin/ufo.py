#!/usr/bin/env python

import sys
import os
import os.path
import atexit
import tempfile
import shutil
import subprocess as sub
import threading
import signal
import resource 

root = os.path.dirname (os.path.dirname (os.path.realpath (__file__)))
verbose = True

cil_m32_model = "short=2,2 int=4,4 long=4,4 long_long=8,4 pointer=4,4 alignof_enum=4 float=4,4 double=8,4 long_double=12,4 void=1 bool=1,1 fun=1,1 alignof_string=1 max_alignment=16 size_t=unsigned_long wchar_t=int char_signed=true const_string_literals=true big_endian=false __thread_is_keyword=true __builtin_va_list=true underscore_name=false"

cil_m64_model = "short=2,2 int=4,4 long=8,8 long_long=8,8 pointer=8,8 alignof_enum=4 float=4,4 double=8,8 long_double=16,16 void=1 bool=1,1 fun=1,1 alignof_string=1 max_alignment=16 size_t=unsigned_long wchar_t=int char_signed=true const_string_literals=true big_endian=false __thread_is_keyword=true __builtin_va_list=true underscore_name=false"

running_process = None

def isexec (fpath):
    if fpath == None: return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK) 

def which(program):
    fpath, fname = os.path.split(program)
    if fpath:
        if isexec (program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if isexec (exe_file):
                return exe_file
    return None

def kill (proc):
    try:
        proc.terminate ()
        proc.kill ()
        proc.wait ()
        global running_process
        running_process = None
    except OSError:
        pass

def loadEnv (filename):
    if not os.path.isfile (filename): return

    f = open (filename)
    for line in f:
        sl = line.split('=', 1)
        # skip lines without equality
        if len(sl) != 2: 
            continue
        (key, val) = sl

        os.environ [key] = os.path.expandvars (val.rstrip ())
        
def parseOpt (argv):
    from optparse import OptionParser
    
    parser = OptionParser ()
    parser.add_option ('-o', dest='out_name', 
                       help='Output file name')
    parser.add_option ("--lawi", dest="lawi")
    parser.add_option ("--install", dest="install", 
                       help="Directory to install UFO distribution into",
                       default=None)
    parser.add_option ("--save-temps", dest="save_temps",
                       help="Do not delete temporary files",
                       action="store_true",
                       default=False)
    parser.add_option ("--temp-dir", dest="temp_dir",
                       help="Temporary directory",
                       default=None)
    parser.add_option ("--mark-lines", dest="mark_lines",
                       help="Insert line markers for counterexample trace",
                       default=False, action='store_true')
    parser.add_option ("--time-passes", dest="time_passes", 
                       help="Time LLVM passes",
                       default=False, action='store_true')
    parser.add_option ('--no-ufo', action='store_false', 
                       dest='do_ufo', 
                       help='Only pre-process the files',
                       default=True)
    parser.add_option ('--no-opt', action='store_false', dest='do_opt',
                       help='Do not do final optimization', default=True)
    parser.add_option ('--no-cil', action='store_false', dest='do_cil',
                       help='Do not run cil on the input file',
                       default=True)
    parser.add_option ('-O', type='int', dest='L',
                       help='Optimization level L:[0,1,2,3]', default=3)
    parser.add_option ('-m', type='int', dest='machine',
                       help='Machine architecture MACHINE:[32,64]', default=32)
    parser.add_option ('--cpu', type='int', dest='cpu',
                       help='CPU time limit (seconds)', default=-1)
    parser.add_option ('--mem', type='int', dest='mem',
                       help='MEM limit (MB)', default=-1)
    parser.add_option ('--cex', dest='cex',
                       help='Destination for a cex', 
                       default=None)

    (options, args) = parser.parse_args (argv)

    if options.install == None and len (args) != 1:
        parser.error ("Specify a single file to verify")

    if options.do_ufo and not options.do_opt:
        parser.error ('Cannot verify without optimizing. ' + 
                      'Either add --no-ufo or remove --no-opt')

    if options.L < 0 or options.L > 3:
        parser.error ("Unknown option: -O%s" % options.L)
    
    if options.machine != 32 and options.machine != 64:
        parser.error ("Unknown option -m%s" % options.machine)

    if options.cex != None:
        if os.path.isfile (options.cex): os.remove (options.cex)
        
    return (options, args)

def createWorkDir (dname = None, save = False):    
    if dname == None:
        workdir = tempfile.mkdtemp (prefix='ufo-')
    else:
        workdir = dname

    if verbose:
        print "Working directory", workdir

    if not save:
        atexit.register (shutil.rmtree, path=workdir)
    return workdir

def getGcc ():
    gcc = which ("gcc")
    if gcc == None:
        raise IOError ("Cannot find gcc")
    return gcc

def getLlvmGcc ():
    llvmgcc = None
    if 'LLVM_GCC' in os.environ:
        llvmgcc = os.environ ["LLVM_GCC"]
    if not isexec (llvmgcc):
        llvmgcc = os.path.join (root, "tools/llvm-gcc/bin/llvm-gcc")
    if not isexec (llvmgcc):
        llvmgcc = os.path.join (root, "bin/llvm-gcc")
    if not isexec (llvmgcc):
        llvmgcc = which ("llvm-gcc")
    if not isexec (llvmgcc):
        raise IOError ("Cannot find llvm-gcc")

    return llvmgcc

def getCillyAsmExe ():
    cilly = os.path.join (root, "tools/cil/obj/x86_LINUX/cilly.asm.exe")
    if not isexec (cilly):
        cilly = os.path.join (root, "tools/cil/obj/x86_DARWIN/cilly.asm.exe")
    if not isexec (cilly):
        cilly = os.path.join (root, "bin/cilly.asm.exe")
    if not isexec (cilly):
        raise IOError ("Cannot find cilly.asm.exe")
    return cilly

def getOpt ():
    opt = None
    if 'OPT' in os.environ:
        opt = os.environ ['OPT']
    if not isexec (opt):
        opt = os.path.join (root, "bin/opt-2.6")
    if not isexec (opt):
        raise IOError ("Cannot find opt")
    return opt

def getBrunch ():
    brunch = os.path.join (root, "bin/brunch.py")
    if not isexec (brunch):
        raise IOError ("Could not find brunch")
    return brunch

def getRunBench ():
    bench = os.path.join (root, "bench-scripts/run-bench.py")
    if not isexec (bench):
        bench = os.path.join (root, "bin/run-bench.py")
    if not isexec (bench):
        raise IOError ("Coud not find run-bench.py")
    return bench

def getUfo ():
    ufo = None
    if 'UFO' in os.environ: ufo = os.environ ['UFO']
    if not isexec (ufo): 
        ufo = os.path.join (root, "bin/ufo")
    if not isexec (ufo):
        raise IOError ("Cannot find ufo")
    return ufo

### Passes
def defCppName (name, wd=None): 
    base = os.path.basename (name)
    if wd == None: wd = os.path.dirname  (name)
    fname = os.path.splitext (base)[0] + '.i'
    return os.path.join (wd, fname)
def defUfoName (name, wd=None): 
    base = os.path.basename (name)
    if wd == None: wd = os.path.dirname  (name)
    fname = os.path.splitext (base)[0] + '.ufo'    
    return os.path.join (wd, fname)
def defBcName (name, wd=None): 
    base = os.path.basename (name)
    if wd == None: wd = os.path.dirname  (name)
    fname = os.path.splitext (base)[0] + '.bc'
    return os.path.join (wd, fname)
def defOBcName (name, wd=None): 
    base = os.path.basename (name)
    if wd == None: wd = os.path.dirname  (name)
    fname = os.path.splitext (base)[0] + '.o.bc'
    return os.path.join (wd, fname)
def defOptBcName (name, optLevel=3, wd=None): 
    base = os.path.basename (name)
    if wd == None: wd = os.path.dirname  (name)
    fname = os.path.splitext (base)[0] + '.o{}.bc'.format (optLevel)
    return os.path.join (wd, fname)

def cpp (in_name, out_name, arch=32, extra_args=[]):
    if out_name == '' or out_name == None:
        out_name = defCppName (in_name)

    gcc_args = [getGcc (), 
                '-D_GNUCC', '-E', '-m{}'.format (arch), '-DCIL=1', '-xc',
                '-o', out_name, in_name ]
    gcc_args.extend (extra_args)

    if verbose: print ' '.join (gcc_args)
    sub.check_call (gcc_args)
    
def cilly (in_name, out_name, extra_args=[], arch=32, mark_lines=False):
    if out_name == '' or out_name == None: out_name = defUfoName (in_name)

    cil_args = [getCillyAsmExe (),
                '--out', out_name, 
                '--doufo', '--noInsertImplicitCasts',
                '--noPrintLn', '--useLogicalOperators',
                '--envmachine', in_name]
    if mark_lines:  cil_args.append ('--ufo-mark-lines')
    cil_args.extend (extra_args)
    
    if arch == 32:
        os.environ ["CIL_MACHINE"] = cil_m32_model
    elif arch == 64:
        os.environ ["CIL_MACHINE"] = cil_m64_model

    if verbose: print ' '.join (cil_args)
    sub.check_call (cil_args)

def llvmGcc (in_name, out_name, arch=32):
    if out_name == '' or out_name == None: out_name = defUfoName (in_name)

    llvm_gcc_args = [getLlvmGcc (),
                     '-xc', '-m{}'.format (arch), '-emit-llvm', '-c', in_name,
                     '-o', out_name]
    if verbose: print ' '.join (llvm_gcc_args)
    sub.check_call (llvm_gcc_args)

def sharedLib (base):
    ext = '.so'
    if sys.platform.startswith ('darwin'): ext = '.dylib'
    return base + ext

def llvmOptBase (in_name, out_name, machine, opt_level=3, time_passes=False):
    if out_name == '' or out_name == None: out_name = defOBcName (in_name)
    
    opt = getOpt ()
    ufofeLib = sharedLib ('lib/libUfoFe')
    ufofeLib = os.path.join (root, ufofeLib)

    opt0_args = [opt, '--stats', '-f', '-funit-at-a-time',
                 "-load", ufofeLib,
                 '-internalize']
    if machine == 64: opt0_args.extend (['-mem2reg', '-nondet-init', '-instcombine', 
                                         '-scalarrepl', 
                                         '-nondet-init', '-instcombine', 
                                         '-kill-nondet', 
                                         '-O3'])

    opt1_args = [opt, "--stats", "-f", "-funit-at-a-time",
                 "-internalize", "-raiseallocs", "-simplifycfg",
                 "-always-inline", "-inline", "--inline-threshold=9999999",
                 "-globaldce", "-globalopt"]
    opt2_args = [opt, "--stats", "-f", "-funit-at-a-time",
                 "-load", ufofeLib, 
                 "-mem2reg", "-nondet-init", "-instcombine", "-scalarrepl", 
                 "-nondet-init", "-instcombine", "-kill-nondet", 
                 "-simplifycfg", "-simplify-libcalls", 
                 "-o", out_name]
    

    if time_passes:
        opt0_args.append ('-time-passes')
        opt1_args.append ('-time-passes')
        opt2_args.append ('-time-passes')

    if verbose:
        print ' '.join (opt0_args), '<', in_name, '|'
        print ' '.join (opt1_args), '|'
        print ' '.join (opt2_args)
    

    opt0 = sub.Popen (opt0_args, stdin=open (in_name), stdout=sub.PIPE)
    opt1 = sub.Popen (opt1_args, stdin=opt0.stdout, stdout=sub.PIPE)
    opt2 = sub.Popen (opt2_args, stdin=opt1.stdout, stdout=sub.PIPE)
    opt1.stdout.close ()
    output = opt2.communicate () [0]
    
    if opt2.returncode != 0:
        raise sub.CalledProcessError (opt2.returncode, opt2_args)
    

def llvmOpt (in_name, out_name, opt_level=3, time_passes=False, cpu=-1):
    if out_name == '' or out_name == None: 
        out_name = defOptBcName (in_name, opt_level)
    import resource as r    
    def set_limits ():
        if cpu > 0: r.setrlimit (r.RLIMIT_CPU, [cpu, cpu])

    opt = getOpt ()
    opt_args = [opt, "--stats", "-f", "-funit-at-a-time"]
    if opt_level > 0 and opt_level <= 3: 
        opt_args.append ('-O{}'.format (opt_level))
    opt_args.extend (['-strip-dead-prototypes', '-deadtypeelim',
                      '-mergereturn', '-lowerswitch', '-instcombine',
                      '-simplifycfg', '-instcombine',
                      '-die', '-dce', '-lowerswitch',
# one final push to promote more global variables
                      '-globalopt', '-mem2reg', '-instcombine', '-simplifycfg',
                      '-lowerswitch',
                      '-o', out_name ])

    if time_passes: opt_args.append ('-time-passes')

    if verbose: print ' '.join (opt_args)

    opt = sub.Popen (opt_args, stdin=open (in_name), 
                     stdout=sub.PIPE, preexec_fn=set_limits)
    output = opt.communicate () [0]
    
    if opt.returncode != 0:
        raise sub.CalledProcessError (opt.returncode, opt_args)
    

def ufo (in_name, opts, cex = None, cpu = -1, mem = -1):
    def set_limits ():
        if mem > 0:
            mem_bytes = mem * 1024 * 1024
            resource.setrlimit (resource.RLIMIT_AS, [mem_bytes, mem_bytes])
                            
    ufo_args = [getUfo ()]
    # either muz or lawi is needed, but not both.
    # lawi is added by default
    if '-muz' not in opts: ufo_args.append ('--lawi')
    ufo_args.append (in_name)
    if cex != None: ufo_args.append ('--ufo-trace={}'.format (cex))
    ufo_args.extend (opts)
    if verbose: print ' '.join (ufo_args)

    p = sub.Popen (ufo_args, preexec_fn=set_limits)
    
    global running_process
    running_process = p

    timer = threading.Timer (cpu, kill, [p])
    if cpu > 0: timer.start ()
    
    try:
        (pid, returnvalue, ru_child) = os.wait4 (p.pid, 0)
        running_process = None
    
    finally:
        ## kill the timer if the process has terminated already
        if timer.isAlive (): timer.cancel ()

    ## if ufo did not terminate properly, propagate this error code
    if returnvalue != 0: sys.exit (returnvalue)
    

# Install target
    
def install (prefix):
    def mkdir (path): 
        if not os.path.exists (path): os.mkdir (path)

    eprefix = os.path.join (prefix, 'bin')
    lib = os.path.join (prefix, 'lib')
    tools = os.path.join (prefix, 'tools')
    mkdir (prefix)
    mkdir (eprefix)
    mkdir (lib)
    mkdir (tools)
    
    shutil.copy2 (getOpt (), os.path.join (eprefix, "opt"))
    shutil.copy2 (getUfo (), os.path.join (eprefix, "ufo"))
    shutil.copy2 (getCillyAsmExe (), os.path.join (eprefix, "cilly.asm.exe"))
    shutil.copy2 (os.path.join (root, sharedLib ('lib/libUfoFe')),
                  os.path.join (lib, sharedLib ('libUfoFe')))
    shutil.copy2 (__file__, os.path.join (eprefix, 
                                          os.path.basename (sys.argv [0])))
    shutil.copy2 (getBrunch (), os.path.join (eprefix, "brunch.py"))
    shutil.copy2 (getRunBench(), os.path.join (eprefix, "run-bench.py"))
    shutil.copy2 (os.path.join (root, "bin/ufo-svcomp-par.py"),
                  os.path.join (eprefix, "ufo-svcomp-par.py"))

    ## result is PATH/bin/llvm-gcc
    llvm_gcc = getLlvmGcc ()
    ## strip llvm-gcc
    llvm_gcc = os.path.dirname (llvm_gcc)
    ## strip bin
    llvm_gcc = os.path.dirname (llvm_gcc)
    print 'Consider copying or linking llvm-gcc using: '
    print 'cp -fav {0} {1}'.format (llvm_gcc, 
                                    os.path.join (tools, "llvm-gcc"))

def is_ufo_opt (x): 
    if x.startswith ('-'):
        y = x.strip ('-')
        return y.startswith ('ufo') or y == 'lawi' or y.startswith ('muz')
    return False
        
def is_non_ufo_opt (x): return not is_ufo_opt (x)
     
def main (argv):
    os.setpgrp ()
    loadEnv (os.path.join (root, "env.common"))

    ufo_args = filter (is_ufo_opt, argv [1:])
    argv = filter (is_non_ufo_opt, argv [1:])
    (opt, args) = parseOpt (argv)

    
    if opt.install != None:  return install (opt.install)
    

    workdir = createWorkDir (opt.temp_dir, opt.save_temps)
    
    assert len(args) == 1
    in_name = args [0]
    
    do_cpp = do_cilly = do_bc = do_base_opt = do_opt = True
    do_opt = opt.do_opt
    do_ufo = opt.do_ufo

    
    (in_base, in_ext) = os.path.splitext (in_name)
    if in_ext == '.i': do_cpp = False
    if in_ext == '.ufo': do_cpp = do_cilly = False
    if in_ext == '.bc' : 
        do_cpp = do_cilly = do_bc = False

        snd_ext = os.path.splitext (in_base)[1]
        if snd_ext == '.o':
            do_base_opt = False
        elif snd_ext.startswith ('.o'):
            do_base_opt = do_opt = False


    if do_cpp:
        cpp_out = defCppName (in_name, workdir)
        assert cpp_out != in_name
        cpp (in_name, cpp_out, arch=opt.machine)
        in_name = cpp_out
    
    if do_cilly:
        ufo_out = defUfoName (in_name, workdir)
        cilly (in_name, ufo_out, arch=opt.machine, mark_lines=opt.mark_lines)
        in_name = ufo_out

    if do_bc:
        bc = defBcName (in_name, workdir)
        llvmGcc (in_name, bc, arch=opt.machine)
        in_name = bc
    
    if do_base_opt:
        obc = defOBcName (in_name, workdir)
        llvmOptBase (in_name, obc, opt.machine, time_passes=opt.time_passes)
        in_name = obc

    if do_opt:
        optbc = defOptBcName (in_name, opt.L, workdir)
        llvmOpt (in_name, optbc,
                 opt_level=opt.L, time_passes=opt.time_passes)
        in_name = optbc
    
    if do_ufo:
        ufo (in_name, ufo_args, cex=opt.cex, cpu=opt.cpu, mem=opt.mem)
        in_name = None

    if opt.out_name != None and in_name != None and in_name != args [0]:
        shutil.copy2 (in_name, opt.out_name)

    return 0

def killall ():
    global running_process
    if running_process != None:
        running_process.terminate ()
        running_process.kill ()
        running_process.wait ()
        running_process = None

if __name__ == '__main__':
    # unbuffered output
    sys.stdout = os.fdopen (sys.stdout.fileno (), 'w', 0)
    try:
        signal.signal (signal.SIGTERM, lambda x, y: killall ())
        sys.exit (main (sys.argv))
    except KeyboardInterrupt: pass
    finally: killall ()
