#!/usr/bin/python
#
# A prototype tool/module for the computation of maximum inductive subsets
#
# (c) Anton Belov, 2013

from __future__ import print_function
import re
import sys
import os
import subprocess 
import tempfile

class Assum:
    '''Aggregates the information pertaining to an assumption'''
    def __init__(self, name="", var=0):
        self.name = name
        self.var = var
        self.pre = None      # True when pre, False when post
        self.enabled = None
        self.pair = None        # the matching assumption

    @property
    def lit(self):
        '''Returns the literal of var that depends on whether its pre/post and
        whether its enabled or not'''
        assert self.pre != None and self.enabled != None, "must be initialized"
        sign = 1 if self.enabled else -1
        return sign*self.var if self.pre else -sign*self.var
    
    def prt(self, f=sys.stdout):
        print("%s (var=%d, pre=%s, en=%s, match_name=%s)" % 
              (self.name, self.var, self.pre, self.enabled, 
               self.pair.name if self.pair else "none"), file=f)

class Misper:
    '''
    Aggregates all the functionality related to the computation of MIS
    
    Configuration parameters (members):
        self.verbose = 1                            # verbosity level
        self.muser_opts = "-prog -order 4 -v 3"     # options for muser-2
    '''
    def __init__(self, aiger_root = "../tools/aiger", muser_path = "../tools/muser/muser-2"):
        '''The aiger_path parameter specifies the path to AIGER tools'''
        self.aigtoaig_path = aiger_root + "/aigtoaig"
        if not os.path.exists(self.aigtoaig_path):
            raise ValueError("aigtoaig is not found at %s" % self.aigtoaig_path)
        self.aigtocnf_path = aiger_root + "/aigtocnf"
        if not os.path.exists(self.aigtocnf_path):
            raise ValueError("aigtocnf is not found at %s" % self.aigtocnf_path)
        self.muser_path = muser_path
        if not os.path.exists(self.muser_path):
            raise ValueError("muser-2 is not found at %s" % self.muser_path)
        self.muser_opts = "-prog -order 4 -v 3"
        self.a_map = {}     # key = name, value = Assum 
        self.clauses = []   # list of lists
        self.num_vars = 0   # number of variables (for the headers) 
        self.verbose = 1    # verbosity level
        self.aig_loaded = False
    
    def _run_command(self, command):
        '''A little helper to run commands and pipe their output'''
        if self.verbose >= 2: 
            print("[misper] executing ", command)
        p = subprocess.Popen(command.split(),
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        return iter(p.stdout.readline, '')
    
    def load_aig(self, file_name):
        '''Loads the circuit from the specified file'''
        if not os.path.exists(file_name):
            raise ValueError("aig file %s does not exist" % file_name)
        if self.verbose >= 2:
            print("[misper] loading aig file", file_name)
        # step 1: convert to aag to get the names and variables of assumptions
        for line in self._run_command(self.aigtoaig_path + " -a " + file_name):
            if not line.startswith("i"): continue
            w = line.strip().split()
            if len(w) != 2: 
                raise RuntimeError("weird input line %s in .aag file" % line)
            pre = True if w[1].startswith("pre!") else False if w[1].startswith("post!") else None
            if pre == None: continue
            try: 
                a = Assum(w[1], int(w[0][1:]) + 1)
            except ValueError: 
                raise RuntimeError("couldn't parse input signal %s in .aag" % w[0])
            a.pre = pre
            if a.name in self.a_map:
                raise RuntimeError("dublicate mapping for assumption %s in .aag file" % a.name)
            self.a_map[a.name] = a
            # check for match
            match_name = a.name.replace("pre!", "post!") if pre else a.name.replace("post!", "pre!")
            if match_name in self.a_map:
                match = self.a_map[match_name]
                assert a.pre != match.pre, "pre/post should be opposite here"
                match.pair = a
                a.pair = match
        #for a in self.a_map.values(): a.prt()
        if (len(self.a_map.items()) == 0):
            raise RuntimeError("did not find any pre! or post! assumptions in the input")
        # get the unmatched lemmas -- should only be one, and only post
        unmatched = [a for a in self.a_map.values() if a.pair == None]
        if len(unmatched) > 1 and self.verbose >= 1:
            print("[misper] WARNING: %d unmatched assumptions" % len(unmatched))
            if self.verbose >= 2:
                print("[misper] unmatched assumptions: ", end="")
                for a in unmatched: print(a.name, end=" ")
                print()
        # step 2: convert to cnf to get the clauses
        for line in self._run_command(self.aigtocnf_path + " " + file_name):
            line = line.strip() 
            if len(line) == 0 or line[0] == "c": continue
            if line[0] == "p": self.num_vars = int(line.split()[2])
            if not re.match("([-0-9]+\s+)*([-0-9]+\s*)", line): continue
            self.clauses.append([int(x) for x in line.split()])
        self.aig_loaded = True

    def write_gcnf(self, file_name):
        '''Writes out the current set of clauses, and the assumptions as a group
        CNF formula that can be used to compute half-MIS, i.e. the maximal subset
        of currently enabled post's that is semi-inductive. The gcnf formula is 
        made in the following way: all pre's and all disabled post's go into 
        group 0. The enabled posts are written each into its own group as if they
        are disabled. Thus, the gcnf instance is UNSAT (semi-inductive), and its 
        MUS represents the minimal set of disabled posts that still maintain 
        semi-inductiveness (i.e. the maximal set of enabled posts).'''
        try:
            gcnf_f = open(file_name, "w")
        except IOError:
            raise RuntimeError("couldn't open %s for writing" % file_name)
        # g0 literals are those from pre's, and from disabled posts; other lits 
        # are the rest (enabled posts)
        (g0lits, g1lits) = ([], [])
        for a in self.a_map.values():
            l = g0lits if (a.pre or not a.enabled) else g1lits
            l.append(a.lit)
        print("p gcnf %d %d %d" % 
              (self.num_vars, len(self.clauses)+len(self.a_map), len(self.a_map)), # muser2 doesn't care anyway  
              file=gcnf_f)
        for c in self.clauses: 
            print("{0}", end="", file=gcnf_f)
            for l in c: print(" %d" % l, end="", file=gcnf_f)
            print(file=gcnf_f)
        for l in g0lits:
            print("{0} %d 0" % l, file=gcnf_f)
        for l in sorted(g1lits):
            print("{%d} %d 0" % (-l, -l), file=gcnf_f)
        gcnf_f.close()
        if self.verbose >= 2:
            print("[misper] wrote out gcnf", file_name)

    def write_cnf(self, file_name):
        '''Writes out the current set of clauses, and the assumptions as a CNF
        formula that can be used to test the inductiveness.'''
        try:
            cnf_f = open(file_name, "w")
        except IOError:
            raise RuntimeError("couldn't open %s for writing" % file_name)
        # units to enable/disable assumptions
        lits = [a.lit for a in self.a_map.values()]
        print("p cnf %d %d" % (self.num_vars, len(self.clauses)+len(lits)),  
              file=cnf_f)
        for c in self.clauses: 
            for l in c: print("%d " % l, end="", file=cnf_f)
            print(file=cnf_f)
        for l in lits:
            print("%d 0" % l, file=cnf_f)
        cnf_f.close()
        if self.verbose >= 2:
            print("[misper] wrote out cnf", file_name)

    def run_muser(self, file_name):
        '''Runs muser2 on the specified .gcnf file. Returns the list of integer
        group-IDs in the computed MUS, or None if something went wrong'''
        if self.verbose >= 2:
            print("[misper] executing muser2 on", file_name)
        res = None
        for line in self._run_command(self.muser_path + " -grp -comp " + 
                                      self.muser_opts + " " + file_name):
            line = line.strip()
            if self.verbose >= 2:
                print("[misper] [muser2]", line)
            if not line.startswith("v "): continue
            try:
                res = [int(x) for x in line.split()[1:-1]]
            except ValueError:
                raise RuntimeError("couldn't parse the output of muser2")
        return res
    
    def compute_halfmis(self):
        '''Computes the returns the maximal subset of currently enabled posts
        that is semi-inductive'''
        gcnf_file = tempfile.mkstemp(".gcnf", text = True)[1]
        self.write_gcnf(gcnf_file)
        res = self.run_muser(gcnf_file)
        os.remove(gcnf_file)
        return [a for a in self.a_map.values() if 
                (not a.pre and a.enabled and a.var not in res)]
    
    def is_inductive(self):
        '''Tests inductiveness of the current set of lemmas. Returns True/False
        or None in case of error'''
        cnf_file = tempfile.mkstemp(".cnf", text = True)[1]
        self.write_cnf(cnf_file)
        res = None
        # just use muser's -ichk    
        for line in self._run_command(self.muser_path + " -ichk -nomus " + cnf_file):
            line = line.strip()
            if line == "ERROR: the instance is SATISFIABLE.":
                res = False
                break
            elif line.startswith("c Initial (UN)SAT check completed"):
                res = True
                break
        os.remove(cnf_file)
        return res
        
    def compute(self):
        '''Computes the MIS. Assumes the aig has already been loaded. Returns
        a tuple (pres,posts) of inductive lemmas'''
        assert self.aig_loaded, "aig should be loaded by now"
        # enable everything, except unmatched pre's
        (pres, posts) = ([], [])  
        for a in self.a_map.values():
            a.enabled = (a.pair != None) or ((a.pair == None) and not a.pre)
            if a.enabled:
                (pres if a.pre else posts).append(a)
        while not self.is_inductive():
            if self.verbose:
                print("[misper] %d lemmas -- the set is not inductive" % len(posts))
            hm = self.compute_halfmis();
            if self.verbose:
                print("[misper] half-MIS size: %d" % len(hm))
            # disable both sides
            for a in posts:
                if a not in hm:
                    a.enabled = False
                    if a.pair != None: 
                        a.pair.enabled = False
                        pres.remove(a.pair)
            posts[:] = hm
        if self.verbose:
            print("[misper] %d lemmas -- the set is inductive." % len(posts))
        return (pres, posts)

    def prt_state(self):
        '''Pretty-prints out the enabled/disabled assertions'''
        an = [a.name for a in self.a_map.values() if not a.pre]
        for n in sorted(an):
            a = self.a_map[n]
            print(" ON: " if a.enabled else "OFF: ", end="")
            print("n/a" if a.pair == None else a.pair.name, end="")
            print(" --", a.name)
        
    def write_smt2(self, f=sys.stdout):
        '''Writes out the current state as a set of SMT2 assertions'''
        for a in self.a_map.values():
            print("(assert %s)" % (a.name if (a.pre == a.enabled) else "(not "+a.name+")"), file=f)

    def get_mis(self):
        '''Returns MIS as a list of pre-post pairs (strings); if the post for the 
        error location is in MIS'''
        return ([], []) # TEMP
     
# TEMP ...
if __name__ == "__main__":
    def parseArgs(argv):
        import argparse as a
        p = a.ArgumentParser(description="MIS extractor")
        p.add_argument ("aig_file", type=str, 
                        help=".aig file with the circuit representation of the bitblasted LA invariant")
        p.add_argument ("-v", "--verb", type=int,
                        help="verbosity level (0,1,2), [def: 1]", default=1)
        p.add_argument ("--smt2", 
                        help="print the answer as a set SMT2 assertions as well [def: off]", 
                        action='store_true', default=False)
        p.add_argument ("--aroot", type=str, 
                        help="specify the path to aiger package root [def: ../tools/aiger]", 
                        default="../tools/aiger")
        p.add_argument ("--mpath", type=str, 
                        help="specify the path to muser-2 executable [def: ../tools/muser-2/muser-2]", 
                        default="../tools/muser-2/muser-2")
        return p.parse_args (argv)
    # go ...
    args = parseArgs(sys.argv[1:])
    try:
        m = Misper(args.aroot, args.mpath)
        m.verbose = args.verb
        m.load_aig(args.aig_file)
        (pres, posts) = m.compute()
        print("[misper] computed MIS of size %d lemmas:" % len(pres))
        m.prt_state()
        if args.smt2:
            m.write_smt2()
    except Exception as e:
        print("[misper] ERROR: " + e.message)
    
