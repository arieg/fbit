#!/usr/bin/env python

import sys
import z3
import z3_utils as z3u


def parseArgs (argv):
    import argparse as a
    p = a.ArgumentParser (description='latobv Linear Arithmetic to BitVector')
    p.add_argument ('-w', '--width', metavar='BITS', type=int, 
                    help='Default bit-width', default=32)
    p.add_argument ('file', metavar='FILE', type=str, 
                    help='Input file')
    p.add_argument('--horn', action='store_true',
                    help='Assume the input is Horn SMT; output will be Horn SMT as well', default=False)
    p.add_argument('--break-and', action='store_true',
                    help='Break the top-level conjunction into separate (assert) statements', default=False)
    p.add_argument ('-o', '--out', metavar='FILE', type=str, 
                    help='Output file', default='-')
    p.add_argument ('--blast', action='store_true',
                    help='Bit blast (non-Horn only)', default=False)
    p.add_argument ('--check-sat', help='Check satisfiability before and after (non-Horn only)',
                    action='store_true', default=False)
    args = p.parse_args (argv)
    return args

class LaToBv (z3u.HelperBase):
    def __init__ (self, ctx):
        z3u.HelperBase.__init__ (self, ctx)
        # all the following fields, except fnum are initialized on a top-level call
        self.visited = None   # cache
        self.bvars = None     # list of replacement bounded vars
        self.rels = None      # list of replacement relations

    def run (self, ast, width=32, top_level=True):
        """Converts all LA terms in a given ast to corresponding bitvector
        terms. Recursive calls should set top_level to False. Note: the context 
        of ast must match self.ctx

        """
        if ast.ctx_ref() != self.ctx.ref():
            raise z3.Z3Exception("Context mismatch: context of ast should match self.ctx")

        #print "Entering with: " + str(ast)
        if top_level:
            self.visited = dict()
            self.bvars = []
            self.rels = []
        
        key = z3u.z3key (ast)
        if key in self.visited: 
            #print "Exiting with (cached): " + str(self.visited[key])
            return self.visited[key]

        # handle bound variables, and store them in self.bvars
        if z3.is_var(ast):
            assert (z3.is_int(ast) or z3.is_real(ast) or z3.is_bool(ast)), \
                    "bound variables can only be numeric or boolean"
            idx = z3.get_var_index(ast)
            if z3.is_bool(ast):
                res = z3.Var(idx, z3.BoolSort(ctx=self.ctx))
            else:
                res = z3.Var(idx, z3.BitVecSort(width,ctx=self.ctx))
            self.bvars.append(res)
        else:
            assert z3.is_app(ast), "must be function application here"
            decl = ast.decl ()
            kind = decl.kind ()
            res = None
            kids = [ self.run (c, width, top_level=False) for c in ast.children ()]
            any_bv = any ([z3.is_bv_sort(k.sort() if z3.is_var(k) else k.decl().range()) for k in kids])
            if len (kids) == 0 or any_bv:
                if kind == z3.Z3_OP_ADD:
                    res = reduce (lambda x,y: x+y, kids)
                elif kind == z3.Z3_OP_MUL:
                    res = reduce (lambda x,y: x*y, kids)
                elif kind == z3.Z3_OP_SUB:
                    res = reduce (lambda x,y: x-y, kids)
                elif kind == z3.Z3_OP_DIV:
                    res = reduce (lambda x,y: x/y, kids)
                elif kind == z3.Z3_OP_LT:
                    res = reduce (lambda x,y: x<y, kids)
                elif kind == z3.Z3_OP_LE:
                    res = reduce (lambda x,y: x<=y, kids)
                elif kind == z3.Z3_OP_GT:
                    res = reduce (lambda x,y: x>y, kids)
                elif kind == z3.Z3_OP_GE:
                    res = reduce (lambda x,y: x>=y, kids)
                elif kind == z3.Z3_OP_EQ:
                    res = reduce (lambda x,y: x==y, kids)
                elif kind == z3.Z3_OP_ITE:
                    res = z3.If (kids [0], kids [1], kids [2])
                elif kind == z3.Z3_OP_UMINUS:
                    res = -kids[0]
                elif kind == z3.Z3_OP_UNINTERPRETED:
                    sort = decl.range ()
                    if sort.kind() == z3.Z3_BOOL_SORT: # handle predicates (for Horn SMT)
                        sig = [] # create a new signature
                        for i in range(0, decl.arity()):
                            d = decl.domain(i)
                            if z3.is_arith_sort(d):
                                sig.append(z3.BitVecSort(width, ctx=self.ctx))
                            else:
                                sig.append(d)
                        sig.append(decl.range())
                        func = z3.Function(decl.name(), sig)
                        res = func(*kids)
                        self.rels.append(res)
                    elif decl.arity() == 0: # handle constants (Boolean fall through)
                        if (sort.kind () == z3.Z3_INT_SORT or
                            sort.kind () == z3.Z3_REAL_SORT):
                            res = z3.BitVec ('bv!' + decl.name (), width, ctx=self.ctx)
                    else:
                        assert False, "should not have uninterpreted non-Boolean functions" 
                elif isinstance (ast, z3.RatNumRef):
                    assert ast.denominator_as_long () == 1
                    res = z3.BitVecVal (ast.numerator_as_long (), width, ctx=self.ctx)
                elif isinstance (ast, z3.IntNumRef):
                    res = z3.BitVecVal (ast.as_long (), width, ctx=self.ctx)
            elif len (kids) > 0:
                if z3.is_and (ast): 
                    res = z3.And (*kids)
                elif z3.is_or (ast): 
                    res = z3.Or (*kids)
                elif z3.is_eq(ast):
                    res = kids[0] == kids[1]
                elif kind == z3.Z3_OP_IMPLIES:
                    res = z3.Implies(kids[0], kids[1], ctx=self.ctx)
                else:
                    res = ast.decl () (*kids)
            
            if res == None : res = ast

        # done
        self.visited [key] = res
        #print "Exiting with: " + str(res)
        return res


    def run_horn(self, fp, query, width=32):
        '''Converts all LA terms and relations to BV in all rules of the given
        z3.Fixpoint() object and the given query. Returns a tuple (fixpoint, 
        query)'''
        if fp.ctx.ref() != self.ctx.ref() or query.ctx_ref() != self.ctx.ref():
            raise z3.Z3Exception("Context mismatch: context of fp and query should match self.ctx")
        res_fp = z3.Fixedpoint(ctx=self.ctx)
        rules = fp.get_rules()
        n_rules = 0
        for r in rules:
            # rules are either (universally quantified) rules or facts
            if z3.is_quantifier(r):
                assert r.is_forall(), "rules must be universally quantified formula"
                bv_body = self.run(r.body(), width)
                assert bv_body.decl().kind() == z3.Z3_OP_IMPLIES, \
                       "TEMP: body of quantified formula is expected to be Implies() (FIXME, maybe)"
                # make constants of appropriate type and substitute them
                n_rules += 1
                sub_vars = [None]*len(self.bvars)
                for var in self.bvars:
                    idx = z3.get_var_index(var)
                    name = "bvar!" + str(n_rules) + "!" + str(idx)
                    if z3.is_bool(var):
                        sub_vars[idx] = z3.Bool(name, self.ctx)
                    else:
                        sub_vars[idx] = z3.BitVec(name, width, self.ctx)
                sub_bv_body = z3.substitute_vars(bv_body, *sub_vars)
                res_fp.declare_var(*sub_vars)
                res_fp.register_relation(*[r.decl() for r in self.rels])
                res_fp.add_rule(sub_bv_body.arg(1), sub_bv_body.arg(0))
            else:
                decl = r.decl()
                assert (decl.kind() == z3.Z3_OP_UNINTERPRETED and \
                        decl.range().kind() == z3.Z3_BOOL_SORT), \
                        "facts must be predicates"
                bv_r = self.run(r, width)
                assert len(self.bvars) == 0, \
                       "TEMP: facts should not have bound variables (FIXME)"
                res_fp.register_relation(*[r.decl() for r in self.rels])
                res_fp.fact(bv_r)
        res_query = self.run(query, width)
        return (res_fp, res_query)

    def __call__ (self, ast=None, fp=None, query=None, width=32):
        if (ast is not None) and (fp is None) and (query is None):
            return self.run (ast, width)
        elif (ast is None) and (fp is not None) and (query is not None):
            return self.run_horn(fp, query, width)
        else:
            raise ValueError("invalid set of arguments provided")

class LaToBvExt:
    '''A self-contained wrapper for LaToBv for external users
    '''
    def convert(self, in_file_name, out_file_name, width, break_and=False):
        '''Entry point for external callers to convert formulas'''
        ctx = z3.Context ()
        latobv = LaToBv (ctx)
        fmla = z3.parse_smt2_file (in_file_name, ctx=ctx)
        bv = latobv (ast=fmla, width=width)
        out = open (out_file_name, 'w')
        if z3.is_and(bv) and break_and:
            z3u.to_smtlib(bv.children(), out)
        else:
            z3u.to_smtlib (bv, out)
        out.write ('(check-sat)\n')
        out.close()
    
    def convert_horn(self, in_file_name, out_file_name, width):
        '''Entry-point for external users to convert Horn systems'''
        ctx = z3.Context ()
        latobv = LaToBv (ctx)
        fp = z3.Fixedpoint(ctx=ctx)
        query = fp.parse_file(in_file_name)[0]
        (bv_fp, bv_query) = latobv(fp=fp, query=query, width=width)
        out = open (out_file_name, 'w')
        out.write(str(bv_fp))
        out.write("(query " + bv_query.sexpr() + ")\n")
        out.close() 
    

def main (argv):
    args = parseArgs (argv[1:])

    ctx = z3.Context ()
    latobv = LaToBv (ctx)
    if args.out == '-':
        out = sys.stdout
    else:
        out = open (args.out, 'w')

    if args.horn:
        fp = z3.Fixedpoint(ctx=ctx)
        query = fp.parse_file(args.file)[0]
        (bv_fp, bv_query) = latobv(fp=fp, query=query, width=args.width)
        out.write(str(bv_fp))
        out.write("(query " + bv_query.sexpr() + ")\n")
        out.flush() 
    else:
        # code for regular formulas
        fmla = z3.parse_smt2_file (args.file, ctx=ctx)
        bv = latobv (ast=fmla, width=args.width)
        if args.blast:
            bv = z3u.bitblast (bv)
        if args.check_sat:
            solver = z3.Solver (ctx=ctx)
            solver.add (fmla)
            res_fmla = solver.check ()
            solver = z3.Solver (ctx=ctx)
            solver.add (bv)
            res_bv = solver.check ()
            print "SAT: before `{0}' and after `{1}'".\
                format (res_fmla, res_bv)
                
        if z3.is_and(bv) and args.break_and:
            z3u.to_smtlib(bv.children(), out)
        else:
            z3u.to_smtlib (bv, out)
        # pass the (check-sat line and the comments after it from the input 
        # file (these might contain extra information)
        cseen = False
        for line in open(args.file):
            if line.startswith("(check-sat"):
                cseen = True
                out.write(line)
            elif cseen and line.startswith(";"):
                out.write(line)
        if not cseen: out.write('(check-sat)\n')
        out.flush ()
    # done
    
if __name__ == '__main__':
    sys.exit (main (sys.argv))
