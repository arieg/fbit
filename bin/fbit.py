#!/usr/bin/env python

import sys
import os
import os.path
import atexit
import tempfile
import shutil
import subprocess as sub
import threading
import signal
import ufo as ufo
import time

from ufo import defOptBcName

#
# globals ...
#
root = os.path.dirname (os.path.realpath (__file__))
verbose = True      # TODO: make an option
running = list()    # list of running processes

###############################################################################

def parseOpt (argv):
    from optparse import OptionParser
    
    parser = OptionParser ()
    parser.add_option ('--cex', dest='cex', default=None,
                       help='Destination for a counterexample file')
    parser.add_option ('--spec', default=None, help='Property file TODO: do we need it ?')
    parser.add_option ("--save-temps", dest="save_temps",
                       help="Do not delete temporary files",
                       action="store_true",
                       default=False)
    parser.add_option ("--temp-dir", dest="temp_dir",
                       help="Temporary directory",
                       default=None)
    parser.add_option ('-m', type=int, dest='arch', default=32,
                       help='Architecture (32 [def] or 64)')
    parser.add_option ('--width', type=int, dest='width', default=32,
                       help='Bitwidth [def: 32]')
    parser.add_option ("--llbmc-delay", type=int, dest="llbmc_delay", default=30,
                       help="Delay (secs) for llbmc stage [def: 30]")
    # ANTON: these need to be implemented in mpl, or removed 
    parser.add_option ('--cpu', type='int', dest='cpu',
                       help='CPU time limit (seconds) TEMP: has no effect', default=-1)
    parser.add_option ('--mem', type='int', dest='mem',
                       help='MEM limit (MB) TEMP: has no effect', default=-1)

    (options, args) = parser.parse_args (argv)

    if options.arch != 32 and options.arch != 64:
        parser.error ('Unknown architecture {}'.format (opt.arch))

    if options.cex is not None and os.path.isfile (options.cex):
        os.remove (options.cex)

    ## workarround the property file requirement
    if options.spec is not None:     # ANTON: do we need this ?
        f = open (options.spec, 'r')
        l = f.readline ()
        if l.find ('ERROR') < 0: 
            print 'BRUNCH_STAT Result UNKNOWN'
            sys.exit (3)

    return (options, args)

def createWorkDir (dname = None, save = False):    
    if dname == None:
        workdir = tempfile.mkdtemp (prefix='fbit-')
    else:
        workdir = dname
    if verbose: print "Working directory", workdir
    if not save: atexit.register (shutil.rmtree, path=workdir)
    return workdir

def getPyPath(script):
    fn = os.path.join (root, script)
    if not isexec (fn): raise IOError ("Cannot find " + script)
    return fn

def isexec (fpath):
    if fpath == None: return False
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK) 

def getUfoPy (): return getPyPath("ufo.py")

def cat (in_file, out_file): out_file.write (in_file.read ())

def runPP (workdir, in_name,  arch=32):     # TODO: make this work ...
    '''Runs the .c -> .bc conversion pipeline'''
    print "[fbit] in_name = ", in_name

    cpp_out = ufo.defCppName (in_name, workdir)
    ufo.cpp (in_name, cpp_out, arch=arch)
    in_name = cpp_out
    print "[fbit] in_name = ", in_name    

    ufo_out = ufo.defUfoName (in_name)
    ufo.cilly (in_name, ufo_out, arch=arch, mark_lines=False)
    in_name = ufo_out
    print "[fbit] in_name = ", in_name

    bc = ufo.defBcName (in_name)
    ufo.llvmGcc (in_name, bc, arch=arch)
    in_name = bc
    print "[fbit] in_name = ", in_name

    obc = ufo.defOBcName (in_name, workdir)
    ufo.llvmOptBase (in_name, obc, arch)
    in_name = obc
    print "[fbit] in_name = ", in_name

    opt = ufo.defOptBcName (in_name, optLevel=3)
    ufo.llvmOpt (in_name, opt, opt_level=3)
    in_name = opt
    print "[fbit] in_name = ", in_name
    return in_name

def runProc (args, fname, stdout, stderr):
    '''Kicks off a specified exec (args) on input fname, with specified 
    stdout/stderr
    ''' 
    args += " " + fname
    if verbose: print "[fbit] kicking off " + args
    return sub.Popen (args.split(),
                      stdout=open (stdout, 'w'),
                      stderr=open (stderr, 'w'))
    
def run (workdir, fname, cex = None, arch=32, width=32, cpu=-1, mem=-1, llbmc_delay=30):
    '''llbmc_delay -- delay in seconds for llbmc'''

    print "[fbit] starting run with fname=%s, arch=%d, width=%d" % (fname, arch, width)
    print "BRUNCH_STAT Result UNKNOWN"
    sys.stdout.flush ()

    print "[fbit] running pp" 
    obc_name = runPP (workdir, fname, arch=arch)
    print "[fbit] finished pp, bitcode=%s" % obc_name

    mpl_py = getPyPath("mpl.py")
    llbmc_py = getPyPath("llbmc.py")

    conf_name = ['llbmc', 'mpl-'+str(width)] 
    ufo = list ()
    ufo.append (llbmc_py + ((" --cex " + cex) if cex is not None else "") + " --delay " + str(llbmc_delay))
    ufo.append (mpl_py + " -v 2 -w " + str(width))
    
    # extras for debugging/testing
    '''conf_name.append('mpl-16')
    ufo.append (mpl_py + " -v 2 -w 16")
    conf_name.append('mpl-8')
    ufo.append (mpl_py + " -v 2 -w 8")'''

    name = os.path.splitext (os.path.basename (obc_name))[0]
    stdout = [os.path.join (workdir, name + '_ufo{}.stdout'.format (i)) 
              for i in range(len (ufo))]
    stderr = [os.path.join (workdir, name + '_ufo{}.stderr'.format (i))
              for i in range (len (ufo))]
    
    global running
    running.extend ([runProc (ufo [i], obc_name, stdout[i], stderr [i])
                     for i in range (len (ufo))])

    orig_pids = [p.pid for p in running]
    pids = [p.pid for p in running ]
    pid = -1
    exit_code = 2
    sig = 0
    while len (pids) != 0:
        print "[fbit] running: %r" % pids

        try:
            (pid, returnvalue, ru_child) = os.wait4 (-1, 0)
        except OSError:  # probably got interrupted
            break
        (exit_code, sig) = (returnvalue // 256, returnvalue % 256) 

        print "[fbit] finished pid %d with code %d and signal %d" % (pid, exit_code, sig)

        # if signal is not 0, assume the child got interrupted, and so the result is UNKNOWN
        if sig != 0: exit_code = 2

        pids.remove (pid)
        
        # exit codes: 0 = UNSAFE, 1 = SAFE, 2 = UNKNOWN
        # TODO: should probably ignore 0 from mpl, and 1 from llbmc 
        if exit_code == 0 or exit_code == 1:
            for p in pids:
                try:
                    print "[fbit] trying to kill ", p
                    #os.killpg (p, signal.SIGALRM)
                    #os.killpg (p, signal.SIGINT)
                    os.kill(p, signal.SIGALRM)
                    os.kill(p, signal.SIGINT)
                except OSError: pass
                finally:
                    try:
                        print "[fbit] waiting for ", p         
                        os.waitpid (p, 0)
                    except OSError: pass
            break
        elif exit_code == 2:
            for p in pids:
                try:
                    print '[fbit] waking up ...'
                    os.kill (p, signal.SIGALRM)
                except OSError: pass
    
    if exit_code == 0 or exit_code == 1:
        idx = orig_pids.index (pid)
        cat (open (stdout [idx]), sys.stdout)
        cat (open (stderr [idx]), sys.stderr)
        print 'WINNER: ', ufo [idx]
        print 'BRUNCH_STAT config {0}'.format (idx)
        print 'BRUNCH_STAT config_name {0}'.format (conf_name [idx])
        print 'BRUNCH_STAT Result ' + ('FALSE' if exit_code == 0 else 'TRUE')
    else:  
        print "ALL INSTANCES FAILED"
        print 'Calling sys.exit with {}'.format (returnvalue // 256)
        print 'BRUNCH_STAT Result UNKNOWN'
        sys.exit (returnvalue // 256)

    running[:] = []
    return exit_code

def main (argv):
    os.setpgrp ()

    (opt, args) = parseOpt (argv[1:])

    workdir = createWorkDir (opt.temp_dir, opt.save_temps)
    returnvalue = 0
    for fname in args:
        returnvalue = run(workdir, fname, opt.cex, opt.arch, opt.width, opt.cpu, opt.mem, opt.llbmc_delay)
    return returnvalue

def killall ():
    global running
    for p in running:
        try:
            if p.poll () == None:
                p.terminate ()
                p.kill ()
                p.wait ()
                # no need to kill pg since it kills its children
        except OSError:   pass
    running[:] = []

if __name__ == '__main__':
    # unbuffered output
    sys.stdout = os.fdopen (sys.stdout.fileno (), 'w', 0)
    try:
        signal.signal (signal.SIGTERM, lambda x, y: killall())
        sys.exit (main (sys.argv))
    except KeyboardInterrupt: pass
    finally: killall ()
            
